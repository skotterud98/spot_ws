$(document).ready( function() {

    var cnvs = document.getElementById("cvs");
    var $cnvs = $("#cvs");
    var ctx = cnvs.getContext("2d");
    var inspection_points = [];
    var path = [];

    var socket = io.connect();

    $cnvs.attr({
        "width" : "700",
        "height" : "300"
    });
        
    $cnvs.on("click", function(event) {
        var x = event.pageX - cnvs.offsetLeft;
        var y = event.pageY - cnvs.offsetTop;
        var dotColor;

        if (inspection_points.length === 0)
            dotColor = "green";
        else
            dotColor = "red";

        inspection_points.push([y, x]);
        drawPoints(x, y, dotColor, 4);
    });

    
    socket.on('connect', function() {
        console.log('Connected to socket with id: ' + socket.id);
    });

    socket.on('trajectory', function(_path) {
        console.log(path);
        path = _path;
        drawPath(path, "lightblue", 4);
        $('#command_input').css("display", "inline");
    });

    socket.on('path_info', function(info) {
        $('#path_info').text(info);
    });

    socket.on('alert_msg', function(msg) {
        alert(msg);
    });

    socket.on('odom_msg', function(odom) {
        $('#x').text("x: " + odom["x"] + " [m]");
        $('#y').text("y: " + odom["y"] + " [m]");
        $('#head').text("Heading: " + odom["head"] + " [rad]");
    })

    $('#solve').click(function() {
        if (inspection_points.length > 1) {
            socket.emit('insp_points', inspection_points);
        }
        else
            alert("Needs minimum two points!");
    });
    
    $('#send_trajectory').click(function() {
        if ($('#scale').val() === "")
            alert("Input pixel per meter to send trajectory command!");
        else
            socket.emit("execute_trajectory", {
                "coords": path, 
                "pixel_per_m": $('#scale').val()
            });
    });

    $('#home_position').click(function() {
        socket.emit("home_pos", "-");
    })

    $('#return').click(function() {
        if ($('#return').is(':checked'))
            socket.emit('return_home', 1);
        else
            socket.emit('return_home', 0);
    });

    setInterval(function() {
        socket.emit("odom_ready", "-");
    }, 100);

    
    function drawPoints(x, y, color, radius) {
        ctx.beginPath();
        ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
        ctx.fillStyle=color;
        ctx.fill();
        
        console.log("clicked: "+x+","+y);
    }


    function drawPath(traj, color, linewidth) {
        ctx.beginPath();
        ctx.lineWidth = linewidth
        for (let i = 0; i < traj.length-1; i++) {
            ctx.moveTo(traj[i][1], traj[i][0]);
            ctx.lineTo(traj[i+1][1], traj[i+1][0]);
            ctx.strokeStyle=color;
            ctx.stroke();
        }
    }



    var joystickL, joystickR;
    var joystickL_xDist, joystickL_yDist, joystickR_xDist, joystickR_yDist;
    var joystickL_Pre_xDist, joystickL_Pre_yDist, joystickR_Pre_xDist, joystickR_Pre_yDist;
    var sendJoystickL_xDist, sendJoystickL_yDist, sendJoystickR_xDist, sendJoystickR_yDist;
    var stop, moving;

    createNipple();

    function createNipple () {
        joystickL = nipplejs.create({
            zone: document.getElementById('left_joy'),
            mode: 'static',
            color: 'blue',
            position: { left: '20%', top: '50%' },
            multitouch: true,
        });

        joystickR = nipplejs.create({
            zone: document.getElementById('right_joy'),
            mode: 'static',
            color: 'red',
            position: { left: '80%', top: '50%' },
            multitouch: true
        });
        bindNipple();
    }

    function bindNipple () {
        joystickL.on('start', function (evt, data) {
            var dat = data['position'];
            joystickL_xDist = dat['x'];
            joystickL_yDist = dat['y'];
            stop = false;

        }).on('move', function (evt, data) {
            console.log('move');
            moving = true;
            GetjoystickL_Data(data);
        }).on('end', function (evt, data) {
            console.log('ending');
            moving = false;
            sendJoystickL_xDist = 0;
            sendJoystickL_yDist = 0;
        });

        joystickR.on('start', function (evt, data) {
            var dat = data['position'];
            joystickR_xDist = dat['x'];
            joystickR_yDist = dat['y'];
        }).on('move', function (evt, data) {
            GetjoystickR_Data(data);
        }).on('end', function (evt, data) {
            console.log('ending');
            sendJoystickR_xDist = 0;
            sendJoystickR_yDist = 0;                    
        });
    }

    function GetjoystickL_Data (obj) {
        var dat = obj['position'];
        var x = dat['x'];
        var y = dat['y'];

        sendJoystickL_xDist = Math.round(x-joystickL_xDist);
        sendJoystickL_yDist = Math.round(y-joystickL_yDist);
    }

    function GetjoystickR_Data (obj) {
        var dat = obj['position'];
        var x = dat['x'];
        var y = dat['y'];

        sendJoystickR_xDist = Math.round(x-joystickR_xDist);
        sendJoystickR_yDist = Math.round(y-joystickR_yDist);
    }

    setInterval(function () {
        if(joystickL_Pre_xDist!= sendJoystickL_xDist || joystickL_Pre_yDist != sendJoystickL_yDist || moving){
            joystickL_Pre_xDist = sendJoystickL_xDist;
            joystickL_Pre_yDist = sendJoystickL_yDist;       
            console.log(sendJoystickL_xDist, sendJoystickL_yDist);                 

            socket.emit('left_stick', {
                "x": sendJoystickL_xDist,
                "y": sendJoystickL_yDist
            });
        }

        if(joystickR_Pre_xDist!= sendJoystickR_xDist || joystickR_Pre_yDist != sendJoystickR_yDist){
            joystickR_Pre_xDist = sendJoystickR_xDist;
            joystickR_Pre_yDist = sendJoystickR_yDist;       
            console.log(sendJoystickR_xDist, sendJoystickR_yDist);                 

            socket.emit('right_stick', {
                "x": sendJoystickR_xDist,
                "y": sendJoystickR_yDist
            });
        }

    }, 100);

});



