#!/usr/bin/env python3
'''
Web controller Node for the Boston Dynamics SPOT project 
at Mechatronics Innovation Lab, august 2021

'''

# Python imports
import os
import time
import math
import numpy as np
import cv2
import pyastar
import json
import threading

# Flask specific imports
from flask import Flask, request, redirect, url_for, render_template, Response
from flask_socketio import SocketIO, emit
from werkzeug.utils import secure_filename

# ROS specific imports
import rospy
from cv_bridge import CvBridge
from geometry_msgs.msg import Twist, Pose, PoseArray, Transform
from sensor_msgs.msg import Image
import spot_ros_msgs.msg
import spot_ros_srvs.srv


SLICE_STEP = 15	# Step number for trajectory point-list slicing
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
UPLOAD_FOLDER = '/home/tarjei/spot_ws/src/spot_mil/scripts/static/uploads/'
IMG_BLUR_KERNEL = (9, 9) # Kernel size for the gaussian blur of grayscale image

app = Flask(__name__)
app.secret_key = "secret key"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
socketio = SocketIO(app)


### Helper functions ###

def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def euler_from_quaternion(x, y, z, w):
        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + y * y)
        roll_x = math.atan2(t0, t1)
     
        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2
        pitch_y = math.asin(t2)
     
        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (y * y + z * z)
        yaw_z = math.atan2(t3, t4)
     
        return [roll_x, pitch_y, yaw_z]

def euler_to_quaternion(roll, pitch, yaw):
        qx = np.sin(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) - np.cos(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
        qy = np.cos(roll/2) * np.sin(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.cos(pitch/2) * np.sin(yaw/2)
        qz = np.cos(roll/2) * np.cos(pitch/2) * np.sin(yaw/2) - np.sin(roll/2) * np.sin(pitch/2) * np.cos(yaw/2)
        qw = np.cos(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)

        return [qx, qy, qz, qw]

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)



### Controller objects ###

class VideoController(object):
    def __init__(self):
        self._bridge = CvBridge()
        self._frame = None

        self._img_sub = rospy.Subscriber('spot_image', Image, self._callback)

    def _callback(self, msg):
        # Convert ROS image to cv2 image and rotate 90 degrees cw
        cv_image = cv2.rotate(self._bridge.imgmsg_to_cv2(msg), cv2.ROTATE_90_CLOCKWISE)
        self._frame = cv2.imencode('.jpg', cv_image)[1].tobytes()

    def gen(self):
        while True:
            if self._frame is not None:
                yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + self._frame + b'\r\n')
            time.sleep(0.05)


class TrajectoryController(object):
    def __init__(self):
        self._img = None
        self._img_blur = None
        self._insp_points = np.array([])
        self._trajectory = np.array([])
        self._position = Transform()
        self._return_home = False

        # Service proxies
        self._trajectory_srv_pub = rospy.ServiceProxy("trajectory_cmd", spot_ros_srvs.srv.Trajectory)
        self._global_trajectory_srv_pub = rospy.ServiceProxy("global_trajectory_cmd", spot_ros_srvs.srv.globalTrajectory)

        # Service requests
        self._trajectory_srv_req = spot_ros_srvs.srv.TrajectoryRequest()
        self._global_trajectory_srv_req = spot_ros_srvs.srv.globalTrajectoryRequest()

        # Subscribers
        self._pos_sub = rospy.Subscriber('kinematic_state', spot_ros_msgs.msg.KinematicState, self._robot_pos, queue_size =10)

    def _robot_pos(self, msg):
        self._position = msg.vision_tform_body

    def _trajectory_service(self, trajectory, start, go_home=False):
        ps = PoseArray()  
        
        if not go_home:
            prev_coord = start

            for coord in trajectory:
                pose = Pose()
                quat = euler_to_quaternion(0, 0, round(math.atan2(coord[1]-prev_coord[1], coord[0]-prev_coord[0]) - 0.05, 3))
                
                pose.position.x = coord[0]
                pose.position.y = coord[1]
                pose.orientation.x = quat[0]
                pose.orientation.y = quat[1]
                pose.orientation.z = quat[2]
                pose.orientation.w = quat[3]

                ps.poses.append(pose)
                prev_coord = coord
        else:
            pose = Pose()
            
            pose.position.x = 0
            pose.position.y = 0
            pose.orientation.x = 0
            pose.orientation.y = 0
            pose.orientation.z = 0
            pose.orientation.w = 1

            ps.poses.append(pose)

        self._global_trajectory_srv_req.globalwaypoints.poses = ps.poses

        try:
            rospy.wait_for_service("global_trajectory_cmd", timeout=5)
            self._global_trajectory_srv_pub(self._global_trajectory_srv_req)
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e, end='')    

    def execute(self, pixel_coords, pixel_per_m):
        trajectory = pixel_coords - pixel_coords[0]
        #trajectory[:, [1,0]] = trajectory[:, [0,1]] # Swapping axes to change from (y, x) to (x,y)
        trajectory = np.around(trajectory / pixel_per_m, 2).tolist()
        if self._return_home:
            trajectory.extend(trajectory[::-1])
        print(trajectory)
        self._trajectory_service(trajectory, trajectory[0])

    def readMapImg(self, file_path):
        self._img = cv2.imread(file_path, cv2.IMREAD_GRAYSCALE)
        print(self._img.shape)
        self._img_blur = cv2.GaussianBlur(self._img, IMG_BLUR_KERNEL, cv2.BORDER_DEFAULT) 

    def findPath(self, insp_points):
        if self._img_blur is not None:
            self._insp_points = insp_points
            grid = self._img_blur.astype(np.float32)
            grid[grid < 230] = np.inf
            grid[grid == 255] = 1
            assert grid.min() == 1, 'cost of moving must be at least 1'

            path_tot = np.array([])
            for i in range(insp_points.shape[0] - 1):
                try:
                    path_tot = np.append(path_tot, pyastar.astar_path(grid, insp_points[i], insp_points[i+1], allow_diagonal=True)[::SLICE_STEP])
                except TypeError:
                    return np.array([])
            
            if path_tot[-1] is not insp_points[-1]:
                path_tot = np.append(path_tot, insp_points[-1])

            path_tot = np.reshape(path_tot, (int(path_tot.size / 2), 2)).astype(int)
            return path_tot
        else:
            return None
    
    def getPos(self):
        return self._position
    
    def goToStart(self):
        self._trajectory_service([0, 0], [0, 0], go_home=True)

    def returnHome(self, bool_msg):
        self._return_home = bool_msg


class JoystickController(object):
    def __init__(self):
        # Define maximum velocity at which Spot will move
        self._lin_vel = 0.5 # m/s
        self._ang_vel = 0.75
        
        # Service proxies
        self._vel_srv_pub = rospy.ServiceProxy("velocity_cmd", spot_ros_srvs.srv.Velocity)
        self._orientation_srv_pub = rospy.ServiceProxy("orientation_cmd", spot_ros_srvs.srv.Orientation)

        # Service requests
        self._vel_srv_req = spot_ros_srvs.srv.VelocityRequest()
        self._orientation_srv_req = spot_ros_srvs.srv.OrientationRequest()


    def velocity_service(self, joy_x, joy_y):
        twist = Twist()

        if joy_y < 0:
            twist.linear.x = round(-joy_y/50, 2) * self._lin_vel
        elif joy_y > 0:
            twist.linear.x = round(-joy_y/50, 2) * self._lin_vel
        else:
            twist.linear.x = 0
        
        if joy_x > 0:
            twist.angular.z = round(-joy_x/50, 2) * self._ang_vel
        elif joy_x < 0:
            twist.angular.z = round(-joy_x/50, 2) * self._ang_vel
        else:
            twist.angular.z = 0
        
        self._vel_srv_req.velocity.linear = twist.linear
        self._vel_srv_req.velocity.angular = twist.angular

        print(joy_x, joy_y)

        try:
            rospy.wait_for_service("velocity_cmd", timeout=2.0)
            self._vel_srv_pub(self._vel_srv_req)
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e, end='')
    

    def orientation_service(self, joy_x, joy_y):
        ps = PoseArray()  
        pose = Pose()	
        
        if joy_y < 0:
            pose.orientation.y = round(joy_y/50, 2) * 0.4
        elif joy_y > 0:
            pose.orientation.y = round(joy_y/50, 2) * 0.4
        else:
            pose.orientation.y = 0
        
        if joy_x > 0:
            pose.orientation.z = round(-joy_x/50, 2) * 0.4
        elif joy_x < 0:
            pose.orientation.z = round(-joy_x/50, 2) * 0.4
        else:
            pose.orientation.z = 0
        
        ps.poses.append(pose)
            
        self._orientation_srv_req.orientation.poses = ps.poses

        try:
            rospy.wait_for_service("orientation_cmd", timeout=5)
            self._orientation_srv_pub(self._orientation_srv_req)
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e, end='')



### Flask server routes ###

@app.route('/')
def upload_form():
	return render_template('index.html')

@app.route('/upload_image', methods=['POST'])
def upload_image():
    if 'file' not in request.files:
        print('No file part')
        return render_template('index.html')
    file = request.files['file']
    if file.filename == '':
        print('No image selected for uploading')
        return render_template('index.html')
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        trajectoryController.readMapImg(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return render_template('index.html', filename=filename)
    else:
        print('Allowed image types are -> .png, .jpg, .jpeg')
        return render_template('index.html')

@app.route('/display_image/<filename>')
def display_image(filename):
	return redirect(url_for('static', filename='uploads/' + filename), code=301)

@app.route('/video_feed')
def video_feed():
    return Response(videoController.gen(), mimetype='multipart/x-mixed-replace; boundary=frame')



### Flask websocket (socketIO) event-handlers ###

@socketio.on('insp_points')
def point_handler(points):
	trajectory = trajectoryController.findPath(np.array(points))

	if trajectory is None:
		emit('path_info', "Unable to read image!")
	elif trajectory.shape[0] >= 1:
		emit('path_info', 'Found path with ' + str(trajectory.shape[0]) + ' waypoints. Refresh page to create a new path.')
		emit('trajectory', trajectory.tolist())
	else:
		emit('path_info', 'No path found! Refresh page and try again.')
		
@socketio.on('execute_trajectory')
def trajectory_handler(data):
    pixel_coords = np.array(data["coords"])
    pixel_per_m = json.loads(data["pixel_per_m"])
    trajectoryController.execute(pixel_coords, pixel_per_m)

@socketio.on('left_stick')
def left_joystick(data):
    joy_x = data["x"]
    joy_y = data["y"]
    joystickController.velocity_service(joy_x, joy_y)

@socketio.on('right_stick')
def left_joystick(data):
    joy_x = data["x"]
    joy_y = data["y"]
    joystickController.orientation_service(joy_x, joy_y)

@socketio.on('odom_ready')
def update_odom(msg):
    pos = trajectoryController.getPos()
    yaw = euler_from_quaternion(pos.rotation.x, pos.rotation.y, pos.rotation.z, pos.rotation.w)[2]

    emit('odom_msg', {
                "x": round(pos.translation.x, 2),
                "y": round(pos.translation.y, 2),
                "head": round(yaw, 2)
            })

@socketio.on('home_pos')
def home_pos(msg):
    trajectoryController.goToStart()

@socketio.on('return_home')
def return_home(state):
    trajectoryController.returnHome(state)



### Main ###

if __name__ == '__main__':
    try:
        # Initialize ROS node in separate thread
        threading.Thread(target=lambda: rospy.init_node('spot_web_controller', disable_signals=True)).start()

        # Class instances
        videoController = VideoController()
        trajectoryController = TrajectoryController()
        joystickController = JoystickController()

        # Run Flask app with the websocket
        socketio.run(app, host='0.0.0.0', port=8000, debug=False)
    
    except rospy.ROSInterruptException:
        pass




